$(document).ready(function () {
    // // TODO: Your code here
    // var x = $("input[type*=radio]");
    // var count = 0;
    // for(let i = 0; i<x.length;i++)
    // {
    //     if(x[i] == $("input:checked")[0]) {
    //         count = i;
    //         break;
    //     }
    // }
    // $("#background")
    //     .change(function(){
    //         $("#background").src="../images/background"+ (count+1) +".jpg";
    //     })
    // .change();

    function updateBackground() {
        $('input[type=radio]').each(function () {
            if ($(this).is(':checked')) {
                let base_name = $(this).attr("id").split("-")[1];
                if ($(this).attr("id") !== "radio-background6") {
                    // set src on background
                    var a = "../images/" + base_name + ".jpg";
                }
                else {
                    a = "../images/" + base_name + ".gif";
                }
                $("#background").attr("src", a);
            }
        });
    }

    function updateAnimals() {
        $("input[type=checkbox]").each(function () {
            let base_name = $(this).attr("id").split("-")[1];
            if (($(this).is(":checked"))) {
                $("#" + base_name).show();
            }
            else {
                $("#" + base_name).hide();
            }
        });
    }

    function changePosition() {
        $("#adjust-components").change(function () {
            let base = $("input[type=range]");
            var x = base[0].value;
            var size = base[1].value;
            var y = base[2].value;
            console.log(size);
            $(".dolphin").each(function () {
                $(this).css("transform", "translateX(" + x + "%) translateY(" + y + "%) scale(" + size + ")");
            });
        });
    }

    $('input[type=radio]').each(function () {
        $(this).on("click", function () {
            updateBackground();
        });
    });
    $('input[type=checkbox]').each(function () {
        $(this).on("click", function () {
            updateAnimals();
        });
    });
    changePosition();
    updateBackground();  //update at first for the default one
    updateAnimals();
});
